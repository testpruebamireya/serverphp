<?php
header('Access-Control-Allow-Origin: *'); 
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

require("Conexion.php"); // importa el archivo de la conexion a la BD
$con = new Conexion();
$conector = new Conector();


$resultadoTodosRegistros=$conector->retornarTodosRegistros();


class Result {}


$json = json_encode($resultadoTodosRegistros); // Muestra el json generado

//Envio de informacion del JSON
header('Content-Type: application/json; charset=utf-8');
echo $json;
?>